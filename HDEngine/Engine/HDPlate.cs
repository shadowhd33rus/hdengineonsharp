﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tao.OpenGl;

namespace HDEngine.Engine
{
    class HDPlate : IDrowable
    {

        public Point3D pos = new Point3D();

        public float size = 1;

        public uint mGlTextureObject = 0;

        public HDPlate()
        {
        }

        public HDPlate(float size)
        {
            this.size = size;
        }

        public HDPlate(Point3D pos, float size):this(size)
        {
            this.pos = pos;
        }
        
        public void LoadTexture(string url, bool smooth)
        {
            mGlTextureObject = Tools.openTexture(url, smooth);
        }
        
        public void Draw(Person person)
        {
            // включаем режим текстурирования
            Gl.glEnable(Gl.GL_TEXTURE_2D);
            // включаем режим текстурирования, указывая идентификатор mGlTextureObject
            Gl.glBindTexture(Gl.GL_TEXTURE_2D, mGlTextureObject);
            Gl.glPushMatrix();

            // отрисовываем полигон
            Gl.glBegin(Gl.GL_QUADS);

            // указываем поочередно вершины и текстурные координаты

            Gl.glTexCoord2f(0, 0);

            Gl.glVertex3d(-size, 0, -size);

            Gl.glTexCoord2f(1, 0);

            Gl.glVertex3d(size, 0, -size);

            Gl.glTexCoord2f(1, 1);

            Gl.glVertex3d(size, 0, size);

            Gl.glTexCoord2f(0, 1);

            Gl.glVertex3d(-size, 0, size);
            // завершаем отрисовку
            Gl.glEnd();

            Gl.glPopMatrix();
            Gl.glDisable(Gl.GL_TEXTURE_2D);
        }
    }
}
