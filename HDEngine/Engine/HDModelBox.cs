﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tao.OpenGl;

namespace HDEngine.Engine
{
    //TODO Create Model Interface
    class HDModelBox : IDrowable
    {
        public Point3D pos = new Point3D();

        public float width = 1;
        public float heidth = 1;
        public float depth = 1;
        public uint mGlTextureObject = 0;

        public HDModelBox()
        {

        }

        public HDModelBox(float width, float heidth, float depth)
        {
            this.width = width;
            this.heidth = heidth;
            this.depth = depth;
        }

        public HDModelBox(float size) : this(size, size, size) { }

        public HDModelBox(float width, float heidth, float depth, float posX, float posY, float posZ):this(width,heidth, depth)
        {
            this.pos = new Point3D(posX, posY, posZ);
        }

        public void LoadTexture(string url, bool smooth)
        {
            mGlTextureObject = Tools.openTexture(url, smooth);
        }

        // TODO Insert code form Skybox

        public void Draw(Person person)
        {
            // включаем режим текстурирования
            Gl.glEnable(Gl.GL_TEXTURE_2D);
            // включаем режим текстурирования, указывая идентификатор mGlTextureObject
            Gl.glBindTexture(Gl.GL_TEXTURE_2D, mGlTextureObject);

            Gl.glPushMatrix();

            Gl.glTranslated(pos.x, pos.y, pos.z);

            // FRONT
            Gl.glPushMatrix();
            DrawPlate(0.25f, 1f / 3f, 0.5f, 2f / 3f, width, heidth, depth);
            Gl.glPopMatrix();

            Gl.glRotated(90, 0, 1, 0);
            // LEFT
            Gl.glPushMatrix();
            DrawPlate(0f, 1f / 3f, 0.25f, 2f / 3f, depth, heidth, width);
            Gl.glPopMatrix();

            Gl.glRotated(90, 0, 1, 0);
            // BACK
            Gl.glPushMatrix();
            DrawPlate(0.75f, 1f / 3f, 1f, 2f / 3f, width, heidth, depth);
            Gl.glPopMatrix();

            Gl.glRotated(90, 0, 1, 0);
            // RIGHT
            Gl.glPushMatrix();
            DrawPlate(0.5f, 1f / 3f, 0.75f, 2f / 3f, depth, heidth, width);
            Gl.glPopMatrix();

            Gl.glRotated(90, 0, 1, 0);
            Gl.glRotated(90, 1, 0, 0);
            // TOP
            Gl.glPushMatrix();
            DrawPlate(0.25f, 2f / 3f, 0.5f, 1f, heidth, width, depth);
            Gl.glPopMatrix();

            Gl.glRotated(180, 1, 0, 0);
            // BOTTOM
            Gl.glPushMatrix();
            DrawPlate(0.25f, 0, 0.5f, 1f / 3f, heidth, width, depth);
            Gl.glPopMatrix();

            Gl.glPopMatrix();
            Gl.glDisable(Gl.GL_TEXTURE_2D);
        }

        private void DrawPlate(float textureStartX, float textureStartY, float textureEndX, float textureEndY,
            float d, float h, float w)
        {
            //Gl.glTranslated(0, 0, -d);

            // отрисовываем полигон
            Gl.glBegin(Gl.GL_QUADS);

            // указываем поочередно вершины и текстурные координаты

            Gl.glTexCoord2f(textureStartX, textureStartY);

            Gl.glVertex3d(-w , -h , -d);

            Gl.glTexCoord2f(textureEndX, textureStartY);

            Gl.glVertex3d(w, -h, -d);

            Gl.glTexCoord2f(textureEndX, textureEndY);

            Gl.glVertex3d(w, h, -d);

            Gl.glTexCoord2f(textureStartX, textureEndY);

            Gl.glVertex3d(-w, h, -d);
            // завершаем отрисовку
            Gl.glEnd();
        }

    }
}
