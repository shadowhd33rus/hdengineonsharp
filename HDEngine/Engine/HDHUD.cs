﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tao.OpenGl;

namespace HDEngine.Engine
{
    class HDHUD : IDrowable
    {

        HDModelBox gun = new HDModelBox(3, 0.1f, 0.1f);

        public void Draw(Person person)
        {
            Gl.glLoadIdentity();

            Gl.glPushMatrix();

            Gl.glScalef(0.1f,0.1f,0.1f);
            Gl.glTranslatef(0.6f, -0.3f, 0);
            gun.Draw(person);

            Gl.glPopMatrix();


            //Gl.glEnable(Gl.GL_POINT_SMOOTH);
            Gl.glPointSize(100f);


            Gl.glDisable(Gl.GL_DEPTH_TEST);

            Gl.glBegin(Gl.GL_POINTS);
            Gl.glColor4f(1, 0, 0, 1);
            Gl.glVertex2d(3, 3);
            Gl.glEnd();

            Gl.glEnable(Gl.GL_DEPTH_TEST);
        }
    }
}
