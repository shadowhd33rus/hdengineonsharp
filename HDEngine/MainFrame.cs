﻿using HDEngine.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tao.DevIl;
using Tao.FreeGlut;
using Tao.OpenGl;

namespace HDEngine
{
    public partial class MainFrame : Form
    {
        // START Game Objects

        private Person person = new Person();
        private Skybox skybox = new Skybox();
        private HDModelBox block = new HDModelBox(30,1f,0.5f, 4, 0.5f, 0);
        private HDPlate floor = new HDPlate(50);
        private HDModelBox wall = new HDModelBox(30, 20, 0.5f, 45, 20, 0);
        private HDHUD hud = new HDHUD();

        private Point mainFrameCenter;

        private HDTarget target1 = new HDTarget();
        private List<HDTarget> targets = new List<HDTarget>();

        // Some settings objects
        private SettingsParams settingsParams;
        private bool frozenCur = true;

        // отсчет времени
        float global_time = 0;
        // экземпляра класса Explosion
        private HDExplosion BOOMB = new HDExplosion(1, 10, 1, 300, 500);
        private float BOOMBtrans = 0;

        // генератор случайны чисел
        Random rnd = new Random();

        // END Game Objects

        private float rot = 0;

        private const float PIPower = 180f / (float)Math.PI;

        public MainFrame(SettingsParams settingsParams):this()
        {
            this.settingsParams = settingsParams;
            //800x600 -> 815; 639
            this.Height = settingsParams.Height;
            this.Width = settingsParams.Width;
            setRezolution();
        }

        public MainFrame()
        {
            settingsParams = new SettingsParams();
            InitializeComponent();
            AnT.InitializeContexts();
        }

        private void setRezolution()
        {
            this.AnT.Height = this.Height;
            this.AnT.Width = this.Width;
            mainFrameCenter = new Point(this.Width / 2, this.Height / 2);
        }

        private void MainFrame_Load(object sender, EventArgs e)
        {
            // инициализация бибилиотеки glut
            Glut.glutInit();
            // инициализация режима экрана
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE);

            // инициализация библиотеки OpenIL
            Il.ilInit();
            Il.ilEnable(Il.IL_ORIGIN_SET);

            // установка цвета очистки экрана (RGBA)
            Gl.glClearColor(255, 255, 255, 1);

            // установка порта вывода
            Gl.glViewport(0, 0, AnT.Width, AnT.Height);

            // активация проекционной матрицы
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            // очистка матрицы
            Gl.glLoadIdentity();

            // установка перспективы
            Glu.gluPerspective(settingsParams.viewinAgngle, (float)AnT.Width / (float)AnT.Height, 0.1f, 1000);

            // установка объектно-видовой матрицы
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            // начальные настройки OpenGL
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glEnable(Gl.GL_LIGHT0);

            Gl.glEnable(Gl.GL_BLEND);
            Gl.glBlendFunc(Gl.GL_ONE, Gl.GL_ONE_MINUS_SRC_ALPHA);

            Gl.glAlphaFunc(Gl.GL_GREATER, 0.8f);
            
            
            skybox.LoadTexture(
                (settingsParams.texture) ? AppDomain.CurrentDomain.BaseDirectory + "Resources\\Skybox256.png" : null,
                settingsParams.smoothing);

            block.LoadTexture(
                (settingsParams.texture) ? AppDomain.CurrentDomain.BaseDirectory + "Resources\\CityWall512.jpg" : null,
                settingsParams.smoothing);

            floor.LoadTexture(
                 (settingsParams.texture) ? AppDomain.CurrentDomain.BaseDirectory + "Resources\\Grass1024.jpg": null,
                 settingsParams.smoothing);

            wall.LoadTexture(
                 (settingsParams.texture) ? AppDomain.CurrentDomain.BaseDirectory + "Resources\\CityWall512.jpg": null,
                 settingsParams.smoothing);

            for (int i = 0; i<5; i++)
            {
                targets.Add(new HDTarget(
                 (settingsParams.texture) ? AppDomain.CurrentDomain.BaseDirectory + "Resources\\Target512.png" : null,
                 settingsParams.smoothing));
            }

            target1.LoadTexture((settingsParams.texture) ? AppDomain.CurrentDomain.BaseDirectory + "Resources\\Target512.png" : null,
                 settingsParams.smoothing
                );


            // активация таймера
            renderTimer.Start();

        }
        

        private void Draw()
        {
            /* START Render */

            person.Go();
            // увеличиваем угол поворота
            rot+=0.5f;
            // корректируем угол
            if (rot > 360)
                rot = 0;

            // очистка буфера цвета и буфера глубины
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glLoadIdentity();

            // очищение текущей матрицы
            Gl.glLoadIdentity();


            //Gl.glDisable(Gl.GL_ALPHA_TEST);

            Glu.gluLookAt(person.position.x, person.position.y + person.height, person.position.z,
            person.lookAt.x,
            person.lookAt.y + person.height,
            person.lookAt.z,
            0, 1, 0);

            /* START Skybox render */

            skybox.Draw(person);

            /* END Skybox render */

            // сохраняем состояние матрицы
            Gl.glPushMatrix();

            floor.Draw(person);

            block.Draw(person);

            wall.Draw(person);

            // возвращаем матрицу
            Gl.glPopMatrix();

            Gl.glEnable(Gl.GL_ALPHA_TEST);

            // Далеко в центре
            targetDraw(targets.ElementAtOrDefault(0),
                rot * 4, 40, 1, 0);

            // перемещаются вдоль
            targetDraw(targets.ElementAtOrDefault(1),
                rot * 4, 30, 1, (rot - 180) / 6);
            
            targetDraw(targets.ElementAtOrDefault(2),
                rot * 4, 35, 1, -((rot - 180) / 6));

            // Справа в центре
            targetDraw(targets.ElementAtOrDefault(3),
                -rot, 20, 1, 5);

            // Слева в центре
            targetDraw(targets.ElementAtOrDefault(4),
                 rot + 90, 20, 1, -5);

            Gl.glDisable(Gl.GL_ALPHA_TEST);

            Gl.glPushMatrix();
            Gl.glTranslatef(0, 0, BOOMBtrans);
            // выполняем просчет взрыва
            BOOMB.Calculate(global_time);
            Gl.glPopMatrix();



            // отключаем режим текстурирования
            Gl.glDisable(Gl.GL_TEXTURE_2D);


            // HUD
            hud.Draw(person);



            // обновляем элемент со сценой
            AnT.Invalidate();
        }

        private void targetDraw(HDTarget t, float rotate, float tranX, float tranY, float tranZ)
        {
            if (t != null) t.Draw(person, rotate, tranX, tranY, tranZ);
        }

        private void doBomb(float X, float Y, float Z)
        {
            // устанавливаем новые координаты взрыва
            BOOMB.SetNewPosition(X, Y, Z);
            //BOOMB.SetNewPosition(25, 0, 0);
            // случайную силу
            BOOMB.SetNewPower(rnd.Next(20, 80));
            // и активируем сам взрыв
            BOOMB.Boooom(global_time);
        }

        private void renderTimer_Tick(object sender, EventArgs e)
        {
            Draw();

            // отсчитываем время
            global_time += (float)renderTimer.Interval / 1000;


            //labelDebug.Text = "Person new XY: " + person.lookAtVector.alignXY * 180f / (float) Math.PI + " XZ: " + person.lookAtVector.alignXZ * 180f / (float)Math.PI;
            //labelDebug.Text += "\nPerson old X: " + person.lookAtVector.p.x + " Y: " + person.lookAtVector.p.y + " Z:"+ person.lookAtVector.p.z;
        }

        private void MainFrame_KeyDown(object sender, KeyEventArgs e)
        {

            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.W:
                    person.MoveForward();
                    break;
                case Keys.S:
                    person.MoveBack();
                    break;
                case Keys.D:
                    person.StrafeRight();
                    break;
                case Keys.A:
                    person.StrafeLeft();
                    break;
                case Keys.Space:
                    frozenCur = !frozenCur;
                    break;
            }

        }

        private void MainFrame_MouseMove(object sender, MouseEventArgs e)
        {
            if (frozenCur)
            {
                //labelDebug.Text ="Mouse: "+ e.X + ":"+e.Y;

                person.AddLookAt(mainFrameCenter.X - e.X, mainFrameCenter.Y - e.Y);
                //person.lookAtVector.addVectorCoordsByDergee(mainFrameCenter.X - e.X, mainFrameCenter.Y - e.Y);

                Cursor.Position = this.PointToScreen(mainFrameCenter);
            }
            
        }

        private void MainFrame_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.W:
                case Keys.S:
                    person.MoveNone();
                    break;
                case Keys.D:
                case Keys.A:
                    person.StrafeNone();
                    break;
            }
        }

        private void AnT_Click(object sender, EventArgs e)
        {
            HDTarget t = targets.LastOrDefault();
            if (t != null)
            {
                BOOMBtrans = t.position.z;
                doBomb(t.position.x, 0, t.position.y);
                //doBomb(t.position.x, t.position.y,t.position.z);
                targets.Remove(t);
            }
            
        }

        private void AnT_Scroll(object sender, ScrollEventArgs e)
        {
        }

        private void AnT_MouseClick(object sender, MouseEventArgs e)
        {
            
        }
        
    }
}
