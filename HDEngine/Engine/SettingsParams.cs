﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDEngine.Engine
{
    public class SettingsParams
    {
        public int Width = 1280;
        public int Height = 720;
        public bool smoothing = true;
        public bool texture = true;
        public int viewinAgngle = 45;
    }
}
