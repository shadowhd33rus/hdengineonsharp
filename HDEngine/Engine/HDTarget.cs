﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tao.OpenGl;

namespace HDEngine.Engine
{
    class HDTarget : IDrowable
    {
        public Point3D position = new Point3D();
        private uint mGlTextureObject = 0;

        public HDTarget()
        {
        }

        public HDTarget(string url, bool smooth)
        {
            LoadTexture(url, smooth);
        }

        public void LoadTexture(string url, bool smooth)
        {
            mGlTextureObject = Tools.openTexture(url, smooth);
        }

        public void Draw(Person person, float rotate, float tranX, float tranY, float tranZ)
        {

            Gl.glPushMatrix();
            Gl.glTranslated(position.x = tranX, position.y = tranY, position.z = tranZ);
            // реализуем поворот объекта
            Gl.glRotated(rotate, 0, 1, 0);




            // включаем режим текстурирования
            Gl.glEnable(Gl.GL_TEXTURE_2D);
            // включаем режим текстурирования, указывая идентификатор mGlTextureObject
            Gl.glBindTexture(Gl.GL_TEXTURE_2D, mGlTextureObject);
            // отрисовываем полигон
            Gl.glBegin(Gl.GL_QUADS);

            // указываем поочередно вершины и текстурные координаты
            Gl.glTexCoord2f(0, 0);

            Gl.glVertex3d(-1, -1, 0);

            Gl.glTexCoord2f(1, 0);

            Gl.glVertex3d(1, -1, 0);

            Gl.glTexCoord2f(1, 1);

            Gl.glVertex3d(1, 1, 0);

            Gl.glTexCoord2f(0, 1);

            Gl.glVertex3d(-1, 1, 0);

            // завершаем отрисовку
            Gl.glEnd();

            Gl.glDisable(Gl.GL_TEXTURE_2D);




            // возвращаем матрицу
            Gl.glPopMatrix();
        }

        public void Draw(Person person)
        {

        }
    }
}
