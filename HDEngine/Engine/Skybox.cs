﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tao.OpenGl;

namespace HDEngine.Engine
{
    /// <summary>
    /// Хранит информацию о текстуре окружения.
    /// Может быть Null - тогда рендер скайбокса на локации не будет хранится.
    /// 
    /// Скайбокс отличается от всех остальных графических объектов тем,
    /// что не является физическим, никак не влияет на другие объекты и всегда находится
    /// на максимальном удалении, доступном при выбранном рендеренге.
    /// Каждая локация может иметь свой скайбокс
    /// 
    /// TODO: определиться с тем, как загружается текстура
    /// </summary>
    class Skybox : IDrowable
    {
        private HDModelBox box;

        public Skybox()
        {
            box = new HDModelBox(2);
        }

        public void LoadTexture(string url, bool smooth)
        {
            box.LoadTexture(url, smooth);
        }

        public void Draw(Person person)
        {
            Gl.glDisable(Gl.GL_DEPTH_TEST); // Отключение определения глубины, чтобы скайбокс всегда был сзади
            Gl.glPushMatrix();

            Gl.glTranslatef(person.position.x, person.position.y + person.height, person.position.z);

            box.Draw(person);
 
            Gl.glPopMatrix();
            Gl.glEnable(Gl.GL_DEPTH_TEST);
        }

    }
}
