﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDEngine
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            bool toClose = false;
            List<string> argList = new List<string>();

            Settings settings = new Settings();
            MainFrame mf;

            Application.Run(settings);
            toClose = settings.DialogResult != DialogResult.OK; // закрывается, если в настройках было выбрано не OK

            mf = new MainFrame(settings.settingsParameters);

            if (!toClose)
            {
                Application.Run(mf);
            }
        }
    }
}
