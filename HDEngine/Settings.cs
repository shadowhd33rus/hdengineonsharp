﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDEngine
{
    public partial class Settings : Form
    {
        BindingList<SettButt> buttonsList = new BindingList<SettButt>();
        public Engine.SettingsParams settingsParameters = new Engine.SettingsParams();

        public Settings()
        {
            InitializeComponent();
        }

        private void buttonSaveLoad_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSaveExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
            MessageBoxButtons mbb = MessageBoxButtons.OK;
            string msg = "HDEngine on C#\n" +
                "Сухарников Пётр, ИСПИ \n" +
                "ВлГУ, 2018";
            string cap = "Об авторах";

            MessageBox.Show(msg, cap, mbb);
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            buttonsList.Add(new SettButt("Выход", "ESC"));
            buttonsList.Add(new SettButt("Захват мыши", "Space"));
            buttonsList.Add(new SettButt("Вперёд", "W"));
            buttonsList.Add(new SettButt("Назад", "S"));
            buttonsList.Add(new SettButt("Вправо", "D"));
            buttonsList.Add(new SettButt("Влево", "A"));
            buttonsList.Add(new SettButt("Действие", "ПКМ"));
            dataGridView1.DataSource = buttonsList;

            comboBoxGraphic.SelectedIndex = 1;
            comboBoxQuality.SelectedIndex = 0;
        }

        private void comboBoxGraphic_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxGraphic.SelectedIndex)
            {
                case 0:
                    settingsParameters.Width = 800;
                    settingsParameters.Height = 600;
                    break;
                case 1:
                    settingsParameters.Width = 1280;
                    settingsParameters.Height = 720;
                    break;
                case 2:
                    settingsParameters.Width = 1920;
                    settingsParameters.Height = 1080;
                    break;
                case 3:
                    settingsParameters.Width = 2560;
                    settingsParameters.Height = 1400;
                    break;
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            settingsParameters.viewinAgngle = trackBar1.Value;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            settingsParameters.smoothing = checkBox1.Checked;
        }

        private void comboBoxQuality_SelectedIndexChanged(object sender, EventArgs e)
        {
            settingsParameters.texture = comboBoxQuality.SelectedIndex == 0;
        }
    }

    public class SettButt
    {
        public string action { get; set; }
        public string button { get; set; }


        public SettButt(string action, string button)
        {
            this.action = action;
            this.button = button;
        }

    }

}
